//We use URLSearchParams to access the specific string in the URL
//d1/pages/deleteCourse.html?courseId=asdasdad5a6523213
let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")

//how do we get the token from the localStorage?

let token = localStorage.getItem("token")

//this fetch will be run automatically once you get to the page deleteCourse page.
fetch(`https://radiant-springs-47372.herokuapp.com/api/courses/activate/${courseId}`, {

	method: "PUT",
	headers: {

		"Authorization": `Bearer ${token}`

	}
})
.then(res => res.json())
.then(data => {

	if(data){
		
		alert("Course Activated.")
		window.location.replace("./courses.html")
	} else {

		alert("Something Went Wrong.")
	}


})