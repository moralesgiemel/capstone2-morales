/*
	Once our user reaches the logout page, our script, automatically runs and thus clears and our local storage and redirects to our login page.

*/

//clear storage

localStorage.clear()

//redirect to login page
window.location.replace('./login.html')