

let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem('token')
let userId = localStorage.getItem('id')

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")


fetch(`https://radiant-springs-47372.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price



	//checks if the user is not an Admin
	if(isAdmin!="true")
	{
		//data.enrollees - an array of object of enrolled users
		//data.enrollees[i].userId - an id of an enrolled user
		//data.enrollees.length - number of enrolled users
		//userId - id of logged in user

		//checks if user is enrolled
		let isUserEnrolled = false;
		for(let i = 0; i < data.enrollees.length; i++){

			if(userId==data.enrollees[i].userId)
	  			isUserEnrolled = true;		
		}

		//if enrolled change button
		if(isUserEnrolled){

			enrollContainer.innerHTML = `<button class="btn btn-block btn-success">You are Enrolled in this Course</button>`

		} else {

			enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`
			
			if(token==null)
			{
				document.querySelector("#enrollButton").addEventListener("click",()=>{
					window.location.replace("./register.html")
				})

			} else {	
				//add a click event to our button
				document.querySelector("#enrollButton").addEventListener("click",()=>{

					fetch('https://radiant-springs-47372.herokuapp.com/api/users/enroll', {

						method: 'POST',
						headers: {

							"Content-Type": "application/json",
							"Authorization": `Bearer ${token}`

						},
						body: JSON.stringify({

							courseId: courseId

						})

					})
					.then(res => res.json())
					.then(data => {

						if(data){
							alert("You have enrolled successfully.")
							window.location.replace("./courses.html")
						} else {

							alert("Enrollment Failed.")
						}
						
					})
				})
			}		
			
		}

	} else {	

		//for Admin

		//displays all enrolled users in the course
		let enrolleesData;
		
		enrolleesData = data.enrollees.map(enrollee => {

			fetch(`https://radiant-springs-47372.herokuapp.com/api/users/${enrollee.userId}`)
			.then(res => res.json())
			.then(data => {
			
				enrollContainer.innerHTML += 
				`	
					<div class="col-12">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">${data.firstName} ${data.lastName}</h5>
								<p class="card-text">Enrolled On: ${enrollee.enrolledOn}</p>
								Status: Enrolled
							</div>
						</div>
					</div>

				`						
			})	

		}).join("")
		
		//display if no enrollees
		if(data.enrollees.length<1)
		enrollContainer.innerHTML = 'No Enrollees Yet.'

	} //end of else	
						
})

