let navItems = document.querySelector("#navSession");

let userToken = localStorage.getItem("token");	
let isAdmin = localStorage.getItem("isAdmin")

if(!userToken){
	//this is before we access and add 2 more li to the innerHTML
	console.log(navItems.innerHTML)
	navItems.innerHTML += 
	`
		<li class="nav-item ">
			<a href="./pages/login.html" class="nav-link"> Log in </a>
		</li>
		<li class="nav-item ">
			<a href="./pages/register.html" class="nav-link"> Register </a>
		</li>
	`
} else {


	if(isAdmin=="false"){
		navItems.innerHTML += `

		<li class="nav-item ">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>
		`	
	}

	navItems.innerHTML += `

		<li class="nav-item ">
			<a href="./pages/logout.html" class="nav-link"> Log Out </a>
		</li>
	`
}

/*Mini Activity*/

//Conditionally render a greeting to our user when he is logged in.
//If the user is logged in, show a text in the newly created h3 that says Hello, User!
//If not, show a text that says, Hello, Guest!

//Stretch Goal:
//If the user is logged, check if he is an admin. If the logged in user is an admin, show the text, Hello, Admin!
//If the user is a regular userm show only, Hello, User!
let greeting = document.querySelector("#userGreeting")

//getItem(<nameOfTheKey>)

//data type? getting items from the localstorage, they are strings.

/*if(!userToken) {

	greeting.innerHTML += `Hi Guest`

} else {	
	
	if(isAdmin=="true"){
		greeting.innerHTML += `Hello, Admin!`
	} else {
		greeting.innerHTML += `Hello User!`
	}
	
}*/