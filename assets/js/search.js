
console.log(conditionalFetch)

let searchForm = document.querySelector("#searchForm")
let searchButton = document.querySelector(".searchButton")
let footer = document.querySelector(".footer")

searchForm.addEventListener("submit",(e)=>{
	e.preventDefault();
	/*searchBar = document.querySelector(".searchBar").value*/
	let searchCourse = document.querySelector("#searchBar").value
	console.log(searchCourse)

	fetch('https://radiant-springs-47372.herokuapp.com/api/courses/search', {

		method: 'POST',
		headers: {

			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			name: searchCourse
		})
	})
	.then(res=>res.json())
	.then(data=>{

		console.log("It went to search")

		if(data.length < 1){

			courseData = "Course Not Found"
			/*footer.classList.add("fixed-bottom");*/
			footer.classList += ` fixed-bottom`;
			console.log(footer.classList)

		} else {

			courseData = data.map(course => {

				
					//each item in the array is iterated
					//console.log(course.isActive)

					//conditionally render the content of our cardFooter with buttons that only an admin can see or a button that only a regular user can see.
					//If statement runs IF the current user is a regular user or a guest
					if(isAdmin == "true"){
						//button to archive/deactivate
						cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go To Course</a>`
							
						if(course.isActive)
						{
							cardFooter += `<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block">Archive Course</a>`
						} else {

							cardFooter += `<a href="./activateCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block">Activate Course</a>`	
						}

					} else if((isAdmin == "false" || userToken==null)&& course.isActive=="true"){

						//button to go to specific course
						//What do we call an added value in the url? URL Parameter
						console.log("Code went here")
						console.log(course.isActive)
						cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go To Course</a>`

					}

						

					//will be returned as each item in the new array
					return (

						`
							<div class="col-md-6 my-3"
								<div class="card">
									<div class="card-body">
										<h4 class="card-title text-center">${course.name}</h4><br>
										<h5 class="card-text text-left">Course Description:</h5>
										<p class="card-text text-left">${course.description}</p>
										<p class="card-text text-left">Price: ₱${course.price}</p>
									</div>
									<div class ="card-footer">
										${cardFooter}
									</div>
								</div>
							</div>

						`
						)

				

			}).join("")
		
			//join() - joins all of the array elements/items into a string. The argument passed in the method becomes the separator for each item because by default, each Item is separated by a comma.
				

		}
		let container = document.querySelector("#coursesContainer")

		container.innerHTML = courseData

	})

})