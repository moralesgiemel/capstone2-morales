//gets a string
//let isAdmin = localStorage.getItem('isAdmin')

let addButton = document.querySelector('#adminButton')
let cardFooter // will allow us to add a button at the bottom of each of our courses which can redirect to the specific course.

/*
	Conditionally render an anchor tag inside the addButton IF the logged in user is an admin, if not, the addButton div should be empty.

	The anchor tag should link to the addCourse page.

	stretch goal	
	The anchor tag should have the following classes: btn btn-primary

	take a screenshot of your code as acivity-2-code

*/

//let userToken = localStorage.getItem("token");


if(!userToken) {

	
}

else {


	if(isAdmin=="true"){
		

		addButton.innerHTML += `
		<div class="col-md-2 offset-md-10">	
			<a href="./addCourse.html" class="btn btn-primary">Add New Course</a>
		</div>

		`
 	} else {

 		

 	}

	
}

//fetch('<url>',{parameters: method, headers, body})
//Can we send an Object to the server? No. WE have to stringify it.

//What is the localStorage? storage within the browser.
//What command or method do we use to store date in the local storage of the browser?
//localStorage.setItem(<key>,<value>)
//Can we store ALL data types in localStorage? We can only save/store Strings.
//What method do we use to get data from the localStorage?
//localStorage.getItem(<key>)

//How does this fetch request run? Does it run immediately? It runs after the if-else statement

//should be able to show all ACTIVE courses
//Simple get request:

let conditionalFetch; 

if(isAdmin=="true")
{
	conditionalFetch = `https://radiant-springs-47372.herokuapp.com/api/courses/all`

} else {

	conditionalFetch = `https://radiant-springs-47372.herokuapp.com/api/courses/`
}

fetch(conditionalFetch, {
	headers:{

		"Authorization": `Bearer ${userToken}`
	}
})
.then(res => res.json())
.then(data => {

	console.log("It went to courses")
	console.log(data)
	let courseData;

	//to check if there are any active courses
	if(data.length < 1){

		courseData = "No Courses Available"

	} else {

		courseData = data.map(course => {

			//each item in the array is iterated
			//console.log(course.isActive)

			//conditionally render the content of our cardFooter with buttons that only an admin can see or a button that only a regular user can see.
			//If statement runs IF the current user is a regular user or a guest
			if(isAdmin == "false" || !isAdmin){

				//button to go to specific course
				//What do we call an added value in the url? URL Parameter
			
				cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go To Course</a>`

			} else {

				//button to archive/deactivate
				cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go To Course</a>`
					
				if(course.isActive)
				{
					cardFooter += `<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block">Archive Course</a>`
				} else {

					cardFooter += `<a href="./activateCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block">Activate Course</a>`	
				}
			}


			//will be returned as each item in the new array
			return (

				`
					<div class="col-md-6 my-3"
						<div class="card">
							<div class="card-body">
								<h4 class="card-title text-center">${course.name}</h4><br>
								<h5 class="card-text text-left">Course Description:</h5>
								<p class="card-text text-left">${course.description}</p>
								<p class="card-text text-left">Price: ₱${course.price}</p>
							</div>
							<div class ="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>

				`
				)

		}).join("")
	
		//join() - joins all of the array elements/items into a string. The argument passed in the method becomes the separator for each item because by default, each Item is separated by a comma.

	}

	//console.log(courseData)

	//store the element with the id coursesContainer in a variable
	let container = document.querySelector("#coursesContainer")

	container.innerHTML = courseData
})