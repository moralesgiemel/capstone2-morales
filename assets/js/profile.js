
let token = localStorage.getItem("token")
let userId = localStorage.getItem("id")

let name = document.querySelector("#userName")
let mobileNo = document.querySelector("#mobileNum")
let coursesContainer = document.querySelector("#coursesContainer")

	

fetch('https://radiant-springs-47372.herokuapp.com/api/users/details', {

	headers: {
	
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {

	//console.log(data)

	//console.log(data.enrollments)
	name.innerHTML = `${data.firstName} ${data.lastName}`
	mobileNo.innerHTML = `Mobile Number: ${data.mobileNo}`
	
	let courseData;
	courseData = data.enrollments.map(course => {

		//console.log(course)
		//console.log(course.courseId)	

		fetch(`https://radiant-springs-47372.herokuapp.com/api/courses/${course.courseId}`)
		.then(res => res.json())
		.then(data => {	
			
			console.log("data"+data.name)

			coursesContainer.innerHTML += 

			`
				<div class="col-md-12">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">${data.name}</h5>
							<p>${data.description}</p>
							<p>Date Enrolled: ${course.enrolledOn}</p>
						</div>
					</div>
				</div>
			`
		})

	})


	if(data.enrollments.length<1)
		coursesContainer.innerHTML = 'You are not enrolled in any course yet.'

})




