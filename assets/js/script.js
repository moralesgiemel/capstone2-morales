let navItems = document.querySelector("#navSession");
let pageHeader = document.querySelector(".page-header")

//localStorage => an object used to store information indefinitely locally in our devices
let userToken = localStorage.getItem("token");
let isAdmin = localStorage.getItem("isAdmin")
let currentUrl = window.location.href
 
/*
localStorage {
    token: "23o8ru32hr1h23kjlrhk2j3"
    isAdmin: false,
	getItem: function()
	setItem: function()
}
*/

// `` = backticks/julius back ticks/template literals

//conditional rendering:
//dynamically add or delete an html element based on a condition
//Here we are adding our login and register links in the navbar ONLY if the user is not logged in.
//Otherwise, only the logout link will be shown.

//innerHTML = contains all of the element's children as a string.
if(!userToken) {

	//check which url is active
	if(currentUrl.includes('login.html'))
	{	
		navItems.innerHTML +=
		`
			<li class="nav-item active">
				<a href="./login.html" class="nav-link"> Log in </a>
			</li>
			<li class="nav-item ">
				<a href="./register.html" class="nav-link"> Register </a>
			</li>
		`
	} else if(currentUrl.includes('register.html')) {

		navItems.innerHTML +=
		`
			<li class="nav-item">
				<a href="./login.html" class="nav-link"> Log in </a>
			</li>
			<li class="nav-item active">
				<a href="./register.html" class="nav-link"> Register </a>
			</li>
		`
	} else if(currentUrl.includes('courses.html')) {

		pageHeader.innerHTML = 'Courses'

		navItems.innerHTML +=
		`
			<li class="nav-item">
				<a href="./login.html" class="nav-link"> Log in </a>
			</li>
			<li class="nav-item">
				<a href="./register.html" class="nav-link"> Register </a>
			</li>
		`

	} else {

		navItems.innerHTML += 
		`
			<li class="nav-item">
				<a href="./login.html" class="nav-link"> Log in </a>
			</li>
			<li class="nav-item">
				<a href="./register.html" class="nav-link"> Register </a>
			</li>
		`
	}

} else {

	//conditional rendering for regular
	if(isAdmin=="false"){

		if(currentUrl.includes('profile.html'))
		{
			pageHeader.innerHTML = 'Profile'

			navItems.innerHTML += `

			<li class="nav-item active">
				<a href="./profile.html" class="nav-link"> Profile </a>
			</li>
			`			

		} else if (currentUrl.includes('courses.html')) {

			pageHeader.innerHTML = 'Courses'

			navItems.innerHTML += `

			<li class="nav-item">
				<a href="./profile.html" class="nav-link"> Profile </a>
			</li>
			`				

		} else {

			navItems.innerHTML += `

			<li class="nav-item ">
				<a href="./profile.html" class="nav-link"> Profile </a>
			</li>
			`				
		}
	

	//conditional rendering for an admin

	} else {

		if(currentUrl.includes('courses.html'))
		pageHeader.innerHTML = 'Courses'

	}

	navItems.innerHTML += `

		<li class="nav-item ">
			<a href="./logout.html" class="nav-link"> Log Out </a>
		</li>
	`


	console.log(navItems.innerHTML.value)
}
